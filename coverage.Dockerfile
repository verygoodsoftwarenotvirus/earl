FROM golang:latest
WORKDIR /go/src/gitlab.com/verygoodsoftwarenotvirus/earl

ADD . .
RUN mkdir /coverage
RUN rm -rf ./vendor
RUN GO111MODULE=on go mod vendor

CMD ["go", "test", "-v", "-race", "-coverprofile=/coverage/coverprofile.out", "gitlab.com/verygoodsoftwarenotvirus/earl"]
