# Earl

Earl is a URL building library with an API aiming to be something akin to [URI.js](https://medialize.github.io/URI.js/). Note that "something akin to" != "1:1 port".

Earl is designed to use in things like API clients, where you might have a more-or-less static URL where the service might be found, but you have a variety of paths or query values to pass to that API.
