GOPATH            := $(GOPATH)
# GIT_HASH          := $(shell git describe --tags --always --dirty)
BUILD_TIME        := $(shell date -u '+%Y-%m-%d_%I:%M:%S%p')
TESTABLE_PACKAGES := $(shell go list gitlab.com/verygoodsoftwarenotvirus/earl/...)

SERVER_PRIV_KEY := dev_files/certs/server/key.pem
SERVER_CERT_KEY := dev_files/certs/server/cert.pem
CLIENT_PRIV_KEY := dev_files/certs/client/key.pem
CLIENT_CERT_KEY := dev_files/certs/client/cert.pem

vendor:
	GO111MODULE=on go mod init
	GO111MODULE=on go mod vendor

ci-vendor:
	GO111MODULE=on go mod vendor

.PHONY: revendor
revendor:
	rm -rf vendor go.{mod,sum}
	$(MAKE) vendor

.PHONY: prerequisites
prerequisites: vendor $(SERVER_PRIV_KEY) $(SERVER_CERT_KEY) $(CLIENT_PRIV_KEY) $(CLIENT_CERT_KEY)

dev_files/certs/client/key.pem dev_files/certs/client/cert.pem:
	mkdir -p dev_files/certs/client
	openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout dev_files/certs/client/key.pem -out dev_files/certs/client/cert.pem

dev_files/certs/server/key.pem dev_files/certs/server/cert.pem:
	mkdir -p dev_files/certs/server
	openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout dev_files/certs/server/key.pem -out dev_files/certs/server/cert.pem

.PHONY: coverage
coverage:
	if [ -f coverage.out ]; then rm coverage.out; fi
	echo "mode: set" > coverage.out

	for pkg in $(TESTABLE_PACKAGES); do \
		set -e; go test -coverprofile=profile.out -v -race $$pkg; \
		cat profile.out | grep -v "mode: atomic" >> coverage.out; \
	done
	rm profile.out
	go tool cover -html=coverage.out
	if [ -f coverage.out ]; then rm coverage.out; fi

coverprofile.out:
	rm -f `pwd`/coverprofile.out
	touch `pwd`/coverprofile.out

.PHONY: ci-coverage
ci-coverage:
	docker build --tag earl_coverage --file coverage.Dockerfile .
	docker run --volume=$(GOPATH)/src/gitlab.com/verygoodsoftwarenotvirus/earl:/coverage --rm -t earl_coverage

.PHONY: docker-image
docker-image: prerequisites
	docker build --tag todo:latest --file dockerfiles/Dockerfile .

.PHONY: run
run: docker-image
	docker run --rm todo:latest --publish 443

.PHONY: ci-test
ci-test:
	docker build --tag earl_unit_test --file test.Dockerfile .
	docker run --rm earl_unit_test
