FROM golang:latest
WORKDIR /go/src/gitlab.com/verygoodsoftwarenotvirus/earl

ADD . .
RUN rm -rf ./vendor
RUN GO111MODULE=on go mod vendor

ENTRYPOINT go test -cover -v -count=10 -race `go list gitlab.com/verygoodsoftwarenotvirus/earl/...`
