package earl_test

import (
	"net/url"
	"testing"

	"gitlab.com/verygoodsoftwarenotvirus/earl"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const exampleBaseURL = "http://example.org/foo.html?hello=world"

func mustEarl(t *testing.T, uri string) *earl.Earl {
	e, err := earl.New(uri)
	require.NoError(t, err)
	return e
}

func TestNewEarl(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		_, err := earl.New(exampleBaseURL)
		assert.NoError(t, err)
	})

	T.Run("with error", func(t *testing.T) {
		_, err := earl.New("%20://examp;e.org|")
		assert.Error(t, err)
	})
}

func TestEarl_Username(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, exampleBaseURL)

		expected := "http://fart@example.org/foo.html?hello=world"
		actual := e.Username("fart").URL()

		assert.Equal(t, expected, actual, "")
	})

	T.Run("with password", func(t *testing.T) {
		e := mustEarl(t, "http://user:password@example.org/foo.html?hello=world")

		expected := "http://other:password@example.org/foo.html?hello=world"
		actual := e.Username("other").URL()

		assert.Equal(t, expected, actual, "")
	})

	T.Run("removal with empty string", func(t *testing.T) {
		e := mustEarl(t, "http://fart@example.org/foo.html?hello=world")

		expected := exampleBaseURL
		actual := e.Username("").URL()

		assert.Equal(t, expected, actual, "")
	})
}

func TestEarl_Password(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, "http://fart@example.org/foo.html?hello=world")

		expected := "http://fart:password@example.org/foo.html?hello=world"
		actual := e.Password("password").URL()

		assert.Equal(t, expected, actual, "")
	})
}

func TestEarl_Path(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, exampleBaseURL)

		expected := "http://example.org/farts?hello=world"
		actual := e.Path("/farts").URL()

		assert.Equal(t, expected, actual, "")
	})
}

func TestEarl_AddPath(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, "http://example.org/one/?hello=world")

		expected := "http://example.org/one/two?hello=world"
		actual := e.AddPath("/two").URL()

		assert.Equal(t, expected, actual, "")
	})
}

func TestEarl_Suffix(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, exampleBaseURL)

		expected := "http://example.org/foo.txt?hello=world"
		actual := e.Suffix("txt").URL()

		assert.Equal(t, expected, actual, "")

	})
}

func TestEarl_Fragment(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, "http://example.org/")

		expected := "http://example.org/#fart"
		actual := e.Fragment("fart").URL()

		assert.Equal(t, expected, actual, "")
	})
}

func TestEarl_AddQuery(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		e := mustEarl(t, exampleBaseURL)

		expected := "http://example.org/foo.html?hello=world&key=value"
		actual := e.AddQuery(map[string]string{"key": "value"}).URL()

		assert.Equal(t, expected, actual, "")
	})

	T.Run("with nil", func(t *testing.T) {
		e := mustEarl(t, exampleBaseURL)

		actual := e.AddQuery(nil).URL()

		assert.Equal(t, exampleBaseURL, actual, "")
	})
}

func TestConvertMapToValues(T *testing.T) {
	T.Parallel()

	T.Run("normal operation", func(t *testing.T) {
		input := map[string]string{"key": "value"}
		expected := url.Values(url.Values{"key": {"value"}})

		actual := earl.ConvertMapToValues(input)
		assert.Equal(t, expected, actual, "")
	})
}

// func TestEarlPathBuilding(T *testing.T) {
// 	T.Parallel()

// 	tests := []struct {
// 		prepFunc    func(e *earl.Earl) *earl.Earl
// 		expectation string
// 		msg         string
// 	}{
// 		{
// 			prepFunc: func(e *earl.Earl) *earl.Earl {
// 				return e.Username("fart")
// 			},
// 			expectation: "http://fart@example.org/foo.html?hello=world",
// 			msg:         "Username() should set the username",
// 		},
// 		{
// 			prepFunc: func(e *earl.Earl) *earl.Earl {
// 				return e.Username("")
// 			},
// 			expectation: "http://example.org/foo.html?hello=world",
// 			msg:         "Username() when called with an empty string should clear the username",
// 		},
// 		// {
// 		// 	prepFunc: func(e *earl.Earl) *earl.Earl {
// 		// 		e.Directory("bar")
// 		// 	},
// 		// 	expectation: "http://example.org/foo.html?hello=world/fart",
// 		// },
// 		{
// 			prepFunc: func(e *earl.Earl) *earl.Earl {
// 				return e.Suffix("txt")
// 			},
// 			expectation: "http://example.org/foo.txt?hello=world",
// 			msg:         "Suffix() should change the suffix of the URL",
// 		},
// 		{
// 			prepFunc: func(e *earl.Earl) *earl.Earl {
// 				return e.Fragment("fart")
// 			},
// 			expectation: "http://example.org/foo.txt?hello=world#fart",
// 			msg:         "Fragment() should set the fragment",
// 		},
// 		{
// 			prepFunc: func(e *earl.Earl) *earl.Earl {
// 				return e.Query(map[string]string{"key": "value"})
// 			},
// 			expectation: "http://example.org/foo.txt?hello=world&key=value",
// 			msg:         "Query() should set the relevant queries when not empty",
// 		},
// 		{
// 			prepFunc: func(e *earl.Earl) *earl.Earl {
// 				return e.Query(nil)
// 			},
// 			expectation: "http://example.org/foo.txt",
// 			msg:         "Query() should clear the relevant queries when empty",
// 		},
// 	}

// 	e := buildExample(T)
// 	for _, test := range tests {
// 		e = test.prepFunc(e)
// 		assert.Equal(T, test.expectation, e.URL(), test.msg)
// 	}
// }
