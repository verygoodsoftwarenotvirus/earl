package earl

import (
	"net/url"
	"path"
	"strings"
)

type Earl struct {
	url *url.URL
}

// New takes a URI and returns a new instance of Earl
func New(uri string) (*Earl, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}

	return &Earl{url: u}, nil
}

// URL returns the final constructed URL
func (e *Earl) URL() string {
	return e.url.String()
}

// Username sets the username portion of the URL.
func (e *Earl) Username(username string) *Earl {
	p, ok := e.url.User.Password()
	if ok {
		e.url.User = url.UserPassword(username, p)
		return e
	} else if username != "" {
		e.url.User = url.User(username)
	} else {
		e.url.User = nil
	}

	return e
}

// Password sets the password portion of the URL.
func (e *Earl) Password(password string) *Earl {
	e.url.User = url.UserPassword(e.url.User.Username(), password)
	return e
}

// Path sets the path portion of the URL.
func (e *Earl) Path(p string) *Earl {
	// NOTE: I may regret this later (relative paths may omit leading slash)
	p = ensureStartsWith("/", p)
	if p != "" {
		e.url.Path = p
	}
	return e
}

// AddPath appends to the path portion of the URL
func (e *Earl) AddPath(p string) *Earl {
	// NOTE: I may regret this later (relative paths may omit leading slash)
	p = ensureStartsWith("/", p)
	if p != "" {
		e.url.Path = path.Join(e.url.Path, p)
	}
	return e
}

// Suffix replaces the existing extension in the URL. So if your original
// URL was meant to go to whatever.html, calling earl.Suffix("txt") will
// change it to whatever.txt
func (e *Earl) Suffix(suffix string) *Earl {
	suffix = ensureStartsWith(".", suffix)
	p := e.url.Path
	ext := path.Ext(p)
	if ext != "" {
		e.url.Path = strings.Replace(p, ext, suffix, 1)
	}
	return e
}

// Fragment sets the fragment portion of the URL.
func (e *Earl) Fragment(fragment string) *Earl {
	if fragment != "" {
		// if you mistakenly put an octothorpe in the fragment, it becomes %23
		e.url.Fragment = strings.Replace(fragment, "#", "", -1)
	}
	return e
}

// AddQuery appends to the query portion of the URL
func (e *Earl) AddQuery(values map[string]string) *Earl {
	if values == nil {
		return e
	} else {
		q := e.url.Query()
		for k, v := range values {
			q.Add(k, v)
		}
		e.url.RawQuery = q.Encode()
	}

	return e
}

// helper funcs

// ConvertMapToValues is a handy wrapper for generating url.Values objects
// from simple string-to-string maps. Obviously, query values are meant to be
// arrays, but very frequently search queries are meant to be unique, which
// this function is well suited to.
func ConvertMapToValues(input map[string]string) url.Values {
	out := map[string][]string{}
	for key, value := range input {
		out[key] = []string{value}
	}
	return out
}

func ensureStartsWith(prefix, s string) string {
	if strings.HasPrefix(s, prefix) {
		return s
	} else {
		return prefix + s
	}
}
